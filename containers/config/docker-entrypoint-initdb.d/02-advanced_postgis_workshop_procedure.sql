CREATE OR REPLACE PROCEDURE public.advanced_postgis_workshop(
	IN exercise INTEGER DEFAULT NULL,
	IN force BOOLEAN DEFAULT False
	)
AS $advanced_postgis_workshop$

DECLARE
	menu text;
	alreadyrun boolean;
	BEGIN

	SELECT True FROM pg_catalog.pg_class c 
	JOIN pg_catalog.pg_namespace n ON c.relnamespace = n.oid 
	WHERE nspname = '🐘' AND relname = '🐘' INTO alreadyrun;

	If alreadyrun IS NULL THEN
		RAISE NOTICE '
		╔═══════════════════════════════════════════════════════╗
		║               Advanced Postgis Workshop               ║
		╟───────────────────────────────────────────────────────╢
		║                                                       ║
		║  Welcome, looks like this is the first invocation.    ║
		║  Hang tight, there are some files to grab from the    ║
		║  web. It may take a while.                            ║
		║                                                       ║
		║                                                       ║
		╚═══════════════════════════════════════════════════════╝';

		DO
			$$#!/bin/bash
			curl --silent --no-verbose https://naciscdn.org/naturalearth/packages/natural_earth_vector.gpkg.zip --output /files/natural_earth_vector.gpkg.zip
			unzip -p /files/natural_earth_vector.gpkg.zip packages/natural_earth_vector.gpkg > /files/natural_earth_vector.gpkg
			rm /files/natural_earth_vector.gpkg.zip
		
			curl --silent --no-verbose https://gitlab.com/auchindown/advanced-postgis-workshop-data/-/raw/main/foss4g-2022-firenze/hebron.tar.gz --output /files/hebron.tar.gz
			curl --silent --no-verbose https://gitlab.com/auchindown/advanced-postgis-workshop-data/-/raw/main/foss4g-2022-firenze/kgn21.backup --output /files/kgn21.backup
			curl --silent --no-verbose https://geodata.ucdavis.edu/gadm/gadm4.1/gpkg/gadm41_ITA.gpkg --output /files/gadm41_ITA.gpkg
			mkdir /files/hebron
			tar -xzf  /files/hebron.tar.gz --directory /files/hebron/

			$$ LANGUAGE plsh;

		RAISE NOTICE '
		╔═══════════════════════════════════════════════════════╗
		║                                                       ║
		║ Downloads complete.                                   ║
		║                                                       ║
		╚═══════════════════════════════════════════════════════╝';
		CREATE SCHEMA 🐘;
		CREATE TABLE 🐘.🐘 AS SELECT 1 🐘;
	END IF;


END;
$advanced_postgis_workshop$ LANGUAGE PLPGSQL;
