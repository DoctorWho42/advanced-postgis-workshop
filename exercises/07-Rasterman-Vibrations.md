Rasterman Vibrations: Getting a Good Profile
============================================


## The Scenario: Hill and Gully Trek

You work at a small engineering firm and they have a contract to build a small pipeline in a rural area. The engineers want a simple way to determine the slope in degrees of any given section of pipe as that is a key factor in their planning and design phase. They need to get fairly detailed slope information, not more than 50 meters apart.

The engineers use QGIS and want to see the slope as they draw a line.

### Background

You have a very detailed DEM of the area in question.


## Rasters in your DB: Is this a good idea?

There are pros & Cons and lot of articles on the web. But this is a Postgis workshop, so we're gonna stuff them in.


## Getting the data into your database (or not)

Postgis can store rater data in one of two ways: in-db or out-db. In-db is just as it sounds, everything is in a table in the database. Out-db stores some metadata in the database and keeps pointers to the raster files. Overview are always stored in the database. Overviews are particularly usefull for visualization purposes. They are lower resolution versions of your raster.

As always, we create a schema for our data:

```SQL
CREATE SCHEMA hebron;
```

We're going to drop out of the database environment for a while to use `the raster2pgsql` tool. This tool imports data into the database. As was mentioned prior, data can be stored as in-db or out-db. I'm gonna have two groups, one can store it as in-db, the other can store it as out-db

Drop into the container by doing:

```sh
docker exec -it advanced-postgis-workshop_pgsql_1 bash
```

Then execute either:

```sh
# in-db with automatic tiling & overviews @ levels 8 & 16
raster2pgsql -t auto -l 16,32 -I -F -Y hebron/*.tif hebron.hebron | psql -p 44449 -U workshop
```

OR

```sh
# out-db with automatic tiling & overviews @ levels 8 & 16
raster2pgsql -t auto -l 8,16 -I -F -Y -R hebron/*.tif hebron.hebron | psql -p 44449 -U workshop
```

Add the data to QGIS and zoom to the area.

## The Solution:

First, let us create a pipeline table.

```SQL
CREATE TABLE hebron.pipelines(
 pipename text PRIMARY KEY,
 geom geometry(Linestring, 32733)
 );
```

Draw a line of any length in the area.

We can use a view to breakout the pipeline and return the relevant data. We will create several views so we can keep track of each part


Step 1: Let us st_segmentize the linestring and get the individual points

```SQL
CREATE VIEW hebron.stage_1 AS
SELECT (st_dumppoints(st_segmentize(geom, 50))).* from hebron.pipelines;
```

Now we want to get the elevation of each point.

```SQL
CREATE VIEW hebron.stage_2 AS
SELECT stage_1.path[1] AS path,
st_value(hebron.rast, stage_1.geom) AS elev,
stage_1.geom
FROM (hebron.stage_1
JOIN hebron.hebron ON (st_intersects(hebron.rast, stage_1.geom)));
```
Next we can rebuild each line:

```SQL
CREATE VIEW hebron.stage_3 AS
SELECT format('%s-%s'::text, stage_2.path, lead(stage_2.path) OVER ()) AS path,
(lead(stage_2.elev) OVER () - stage_2.elev) AS vert_dist,
st_distance(stage_2.geom, lead(stage_2.geom) OVER ()) AS dist,
st_makeline(stage_2.geom, lead(stage_2.geom) OVER ()) AS geom
FROM hebron.stage_2;
```
Then with simple trig functions we can get the slope:

```SQL
CREATE VIEW hebron.stage_4 AS
SELECT stage_3.path,
stage_3.vert_dist,
stage_3.dist,
stage_3.geom,
atand((stage_3.vert_dist / stage_3.dist)) AS atand
FROM hebron.stage_3;
```

We can rename this last view to a more usefull name:

```SQL
ALTER VIEW hebron.stage_4 RENAME TO proposed_pipes_slope;
```

You can then style this view so that as proposed pipe routes are drawn, the slope can be displayed either via labels or symbology or both.
