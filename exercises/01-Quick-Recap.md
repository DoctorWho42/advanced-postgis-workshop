Quick Recap: A little revision never hurt
=========================================

## Namespaces
I'm a big fan of Namespaces, and you should be too. Namespaces/Schemas allow us to cleanly separate data based on any arbitrary requiremnts that we may have. Let us create a schema for this exercise:

```SQL
CREATE SCHEMA recap;
```

## Creating tables
Data is stored in tables. Tables can have 0 or more columns (the efficacy of a 0 column table is up for debate). You can define a table using `CREATE TABLE` and then add data to it using `INSERT INTO`. You can also define and insert into a table in one go by doing `CREATE TABLE tablename AS {{some query here}}`.

Let us explore how we got to Firenze, Italy. In the following query, replace the {{}} with the requested information.

```SQL
CREATE TABLE recap.how_far_is_home AS
 WITH
  firenze AS (SELECT 'Firenze, Italy' firenze, st_makepoint(11.25, 43.75) g),
  home AS (SELECT '{{Town Name}}' home, st_makepoint({{Town Longitude}}, {{Town Latitude}}) g)
  SELECT
   format('From %s to %s', home, firenze) info,
   st_makeline(home.g, firenze.g) geo
  FROM firenze
  CROSS JOIN home;
```

## Examining Tables & Simple spatial functions

Let us explore the table we just created.

```SQL
SELECT * FROM recap.how_far_is_home;
```
The asterisk simply returns all columns in a given table.

## How Far Did You Travel?

Let us find the distance from your home town to Firenze. This is simply the length of the line in the table. We use the `st_length` function.

```SQL
SELECT st_length(geo) FROM recap.how_far_is_home;
```
Does this look right? No. the `st_makeline` and `st_makepoint` functions we used earlier returns geometry types. The geometry type assumes everything is on a cartesian plane. PostGIS also has a geography type which will be more usefull in this case. As per the [documentation](https://postgis.net/docs/manual-3.2/using_postgis_dbmanagement.html#PostGIS_Geography), the geography data type provides native support for spatial features represented using "geographic" coordinates (sometimes called "geodetic" coordinates, or "lat/lon", or "lon/lat").

Let us cast/convert the geometry type to a geography and see what happens when we apply the `st_length` function.

```SQL
SELECT
 st_length(geo::geography) lengh_over_spheroid,
 st_length(geo::geography, false) length_over_sphere,
 st_length(geo) not_terribly_useful,
 st_length(st_transform(st_setsrid(geo, 4326), 3857)) webmercator_length
FROM recap.how_far_is_home;
```

What do you notice?

## Altering & Updating tables

Let us add the distance to the table. We will also change the geometry column to a geography column.

```SQL
ALTER TABLE recap.how_far_is_home ALTER COLUMN geo TYPE geography;
ALTER TABLE recap.how_far_is_home ADD COLUMN travel_distance_km int;
UPDATE recap.how_far_is_home SET travel_distance_km = st_length(geo)/1000.0;
```

## Stuff to note

When casting from a geometry to a geography, it assumes the coordinates are in a lat/lon format.

```SQL
SELECT st_astext(st_makepoint(90900,87303)::geography);
```